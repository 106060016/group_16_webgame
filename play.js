var scoreString = '';
var scoreText;
var pause;
var stateText;
var cursors;
var mouseX;
var mouseY;

var showSelect;
var tiptext;
var tiprec;

var showDelete;
var killMode;

var playState={

    preload:function(){

    },

    create:function(){
        game.physics.startSystem(Phaser.Physics.ARCADE);
        this.firingTimer = 0;
        killMode = 1;

        this.background = game.add.sprite(game.world.centerX,game.world.centerY, 'background');
        this.background.anchor.setTo(0.5,0.5);
        this.background.scale.setTo(0.5,0.5);
        //地板
        this.squre = [];
        this.squre.lengh = 60;
        for (var y = 0; y < 5; y++){
            for (var x = 0; x < 10; x++){
                this.squre[10*y+x] = game.add.sprite(130 + x * 60, 160 + y * 60, 'floor');
                this.squre[10*y+x].anchor.setTo(0.5, 0.5);
                this.squre[10*y+x].enableBody = true;
                this.squre[10*y+x].physicsBodyType = Phaser.Physics.ARCADE;
                this.squre[10*y+x].thingonit = false;
            }
        }

        //敵人.group
        this.enemypool = game.add.group();
        this.enemypool.enableBody = true;
        game.physics.enable(this.enemypool, Phaser.Physics.ARCADE);
        this.createenemy();

        //敵人子彈
        this.enemyBullets = game.add.group();
        this.enemyBullets.enableBody = true;
        this.enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.enemyBullets.createMultiple(200, 'enemybullet');
        this.enemyBullets.setAll('anchor.x', 0.5);
        this.enemyBullets.setAll('anchor.y', 1);
        this.enemyBullets.setAll('outOfBoundsKill', true);
        this.enemyBullets.setAll('checkWorldBounds', true);

        //植物.group
        this.plants = [i];
        for(var i = 0; i < 6; i++){
            this.plants[i] =game.add.group();
            this.plants[i].enableBody=true;
            game.physics.enable(this.plants[i], Phaser.Physics.ARCADE);
            this.plants[i].setAll('nohurt', 1);
            this.plants[i].cooldown = 0;
        }
        this.createplant();

        //推車.group
        this.cars =game.add.group();
        this.cars.enableBody=true;
        game.physics.enable(this.cars, Phaser.Physics.ARCADE);
        
        this.createcar();

        //子彈.group
        this.bullets = [i];
        for(var i = 1; i < 4; i++){
            this.bullets[i] = game.add.group();
            this.bullets[i].enableBody = true;
            game.physics.enable(this.bullets[i], Phaser.Physics.ARCADE);
            this.createbullet(i);
        }

        //進度條

        this.progressbar = game.add.sprite(480,90, 'progressbar');
        this.progressbaricon = game.add.sprite(725,100, 'enemy');
        this.progressbaricon.anchor.setTo(0.5, 1);
        this.progressbaricon.scale.setTo(0.04, 0.03);
        this.progressbaricon.enableBody = true;
        game.physics.enable(this.progressbaricon, Phaser.Physics.ARCADE);

        //顯示錢
        this.money = 50;
        this.moneyString = '';
        this.moneyicon = game.add.sprite(570, 10, 'coin');
        this.moneyicon.scale.setTo(0.1, 0.1);
        this.moneyText = game.add.text(650, 10, this.moneyString + this.money, { font: '34px Palatino', fill: '#fff' });
        this.moneytimer = 4500;
        this.moneytimerDelay = 4500;
        
        //顯示分數
        this.score = 0;
        scoreString = 'Score : ';
        scoreText = game.add.text(10, 10, scoreString + this.score, { font: '34px Palatino', fill: '#fff' });

        //顯示植物的價錢           
        this.plant1 = game.add.text(90, 530, '$ 20' , { font: '34px Palatino', fill: '#fff' }); 
        this.plant2 = game.add.text(190, 530, '$ 30' , { font: '34px Palatino', fill: '#fff' });
        this.plant3 = game.add.text(290, 530, '$ 40' , { font: '34px Palatino', fill: '#fff' });
        this.plant4 = game.add.text(390, 530, '$ 50' , { font: '34px Palatino', fill: '#fff' });
        this.plant5 = game.add.text(490, 530, '$ 40' , { font: '34px Palatino', fill: '#fff' });   
        this.plant6 = game.add.text(590, 530, '$ 60' , { font: '34px Palatino', fill: '#fff' });

        //顯示贏或輸
        stateText = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '64px Palatino', fill: '#fff' });
        stateText.anchor.setTo(0.5, 0.5);
        stateText.visible = false;
        this.texttime=0;

        //鏟除生物
        this.shovel = game.add.sprite(700, 450, 'shovel');
        this.shovel.scale.setTo(0.1, 0.1);
        this.shovel.inputEnabled = true;
        this.shovel.events.onInputDown.add(()=>{
        killMode *= -1;
        if(killMode==-1){
            showDelete = game.add.sprite(695, 445, 'delete');
            showDelete.alpha = 0.5;
        }
        else showDelete.destroy();
        }, this);


        //暫停
        pause = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
        pause.onDown.add(this.continue,self);
        cursors = game.input.keyboard.createCursorKeys();

        //滑鼠控制
        game.input.onDown.add(this.mousecontrol,this);

        //升級按鍵
        this.upgrade_btn = game.add.button(680, 520, 'upgrade_btn', this.plantUpgrade, this, 1, 0);

        this.plantName = ['ankh','beetle','lion','mummy','coffin','rock'];
    },
    createenemy:function(){
        //enemys.physicsBodyType = Phaser.Physics.ARCADE;
        this.enemypool.createMultiple(100, 'enemy');
        this.enemypool.setAll('scale.x', 0.06);
        this.enemypool.setAll('scale.y', 0.038);
        this.enemypool.setAll('anchor.x', 0.5);
        this.enemypool.setAll('anchor.y', 0.5);
        this.nextEnemypoolAt = 0;
        if(game.global.level==1)
        this.enemypoolDelay = 3000;
        else if(game.global.level==2)
        this.enemypoolDelay = 2500;
        else
        this.enemypoolDelay = 2000;
        this.enemynum=0;
    },

    createplant:function(){
        this.plantpull = [i];
        for(var i = 0; i < 6; i++){
            if(i == 0)  this.plantpull[i] = game.add.sprite(120, 500, 'ankh')
            if(i == 1)  this.plantpull[i] = game.add.sprite(220, 500, 'beetle');
            if(i == 2)  this.plantpull[i] = game.add.sprite(320, 500, 'lion');
            if(i == 3)  this.plantpull[i] = game.add.sprite(420, 500, 'mummy');
            if(i == 4)  this.plantpull[i] = game.add.sprite(520, 500, 'coffin');
            if(i == 5)  this.plantpull[i] = game.add.sprite(620, 500, 'rock');
            this.plantpull[i].anchor.setTo(0.5, 0.5);
            if(i == 0) this.plantpull[i].scale.setTo(0.03, 0.025);
            else if(i<5) this.plantpull[i].scale.setTo(0.06, 0.06); 
            this.plantpull[i].enableBody = true;
            this.plantpull[i].physicsBodyType = Phaser.Physics.ARCADE;
            this.plantpull[i].beplaced = 0; //beplaced = 0 代表不能移動
        }
    },

    createcar:function(){
        this.hitcar=0;
        for(var i=0;i<5;i++){
            var singlecar = this.cars.create(40, 160+60*i, 'car');
                singlecar.scale.setTo(0.055, 0.055);
                singlecar.anchor.setTo(0.5, 0.5);
                singlecar.hitnum = 3;
        }
    },

    createbullet:function(i){
        if(i == 1){
            this.bullets[i].createMultiple(150, 'beetle_bullet');
            this.bullets[i].setAll('scale.x' ,0.8);
            this.bullets[i].setAll('scale.y' ,0.8);
        }  
        if(i == 2){
            this.bullets[i].createMultiple(150, 'lion_bullet');
            this.bullets[i].setAll('angle' ,270);
            this.bullets[i].setAll('scale.x' ,0.7);
            this.bullets[i].setAll('scale.y' ,0.7);
        }  
        if(i == 3)  {
            this.bullets[i].createMultiple(150, 'mummy_bullet');
            this.bullets[i].setAll('scale.x' ,0.05);
            this.bullets[i].setAll('scale.y' ,0.1);
        } 
        this.bullets[i].setAll('anchor.x', 0.5);
        this.bullets[i].setAll('anchor.y', 0.5);
        this.bullets[i].setAll('outOfBoundsKill', true);
        this.bullets[i].setAll('checkWorldBounds', true);
    },

    mousecontrol :function() {
        //如果滑鼠按下去的位置在 plantpull
        if(killMode==1){
        for(var i = 0; i < 6; i++){
            if((game.input.x - this.plantpull[i].x) < 25 && (game.input.x - this.plantpull[i].x) >= -25 && (game.input.y - this.plantpull[i].y) < 25 &&(game.input.y - this.plantpull[i].y) >= -25){
                if((i==0&&this.money-20>=0)||(i==1&&this.money-30>=0)||(i==2&&this.money-40>=0)||(i==3&&this.money-50>=0)||(i==4&&this.money-40>=0)||(i==5&&this.money-60>=0)){
                    if(game.time.now>this.plants[i].cooldown)this.plantpull[i].beplaced++; //spider 的 beplaced > 0 才能移動
                }   
                
            }
        }
    }


        //如果選中植物
        var flag = 0;
        if(game.input.x>=this.upgrade_btn.x && game.input.x<=this.upgrade_btn.x+this.upgrade_btn.width &&
            game.input.y>=this.upgrade_btn.y && game.input.y<=this.upgrade_btn.y+this.upgrade_btn.height){
                flag = 1;
            }//升級不影響選中狀態
        else if(game.input.x>=this.shovel.x && game.input.x<=this.shovel.x+this.shovel.width &&
            game.input.y>=this.shovel.y && game.input.y<=this.shovel.y+this.shovel.height){
                flag = 0;
            }//鏟除取消選中狀態
        else if(killMode==1){
        for(var j=0;j<50;j++){
            if(game.input.x - this.squre[j].x < 30 && game.input.x - this.squre[j].x >= -30 && game.input.y - this.squre[j].y < 30 && game.input.y - this.squre[j].y >= -30){
                if(this.squre[j].thingonit && !this.plantpull[0].beplaced && !this.plantpull[1].beplaced && !this.plantpull[2].beplaced && !this.plantpull[3].beplaced && !this.plantpull[4].beplaced && !this.plantpull[5].beplaced){
                    pos_x = this.squre[j].x;
                    pos_y = this.squre[j].y;
                    for(var k = 1; k < 5; k++){
                            this.plants[k].forEachAlive(function(pla){
                                if(pla.squrenum==j) {
                                    flag = 1;
                                    pla.isselected = 1;
                                    if(showSelect) showSelect.destroy();
                                    showSelect = game.add.sprite(pos_x, pos_y, 'select');
                                    showSelect.anchor.setTo(0.5, 0.5);
                                    showSelect.alpha = 0.3;
                                }
                                else {
                                    pla.isselected = 0;
                                }
                            });
                        }
                }
                break;
            }
        }
        }
        if(!flag && showSelect){
            showSelect.destroy();
            for(var k = 1; k < 5; k++){
                    this.plants[k].forEachAlive(function(pla){
                        pla.isselected = 0;
                    });
                }
        }

    },

    continue:function(){
        if(game.paused==true){
            game.paused=false;
        }
    },

    plantUpgrade:function(){
    if(showSelect){
        for(var k = 1; k < 5; k++){
            this.plants[k].forEachAlive(function(pla){
                if(pla.isselected && pla.level<3 && ((k==1&&playState.money-20>=0)||(k==2&&playState.money-30>=0)||(k==3&&playState.money-40>=0)||(k==4&&playState.money-30>=0))){
                    pla.level++;
                    if(k==1){
                    pla.firepower += 2.5;
                    pla.live += 2;
                    playState.money -= 20;
                    playState.moneyText.text = playState.moneyString + playState.money;
                    }
                    else if(k==2){
                    pla.firepower += 2.5;
                    pla.live += 2;
                    playState.money -= 30;
                    playState.moneyText.text = playState.moneyString + playState.money;
                    }
                    else if(k==3){
                    pla.firepower += 3;
                    pla.live += 2;
                    playState.money -= 40;
                    playState.moneyText.text = playState.moneyString + playState.money;
                    }
                    else{
                    pla.live += 10;
                    playState.money -= 30;
                    playState.moneyText.text = playState.moneyString + playState.money;
                    }
                }
            });
        }
    }
    },

    update:function(){

        if (mouseX != game.input.x)mouseX = game.input.x;
        if (mouseY != game.input.y)mouseY = game.input.y;

        if (game.time.now > this.firingTimer)
        {
            this.enemyFires();
        }

        //控制文字顯示的時間
        if(game.time.now > this.texttime){
            stateText.visible = false;
            //this.enemypoolDelay = 300;
        }
        
        //進度
        if(game.global.level == 1){
            this.progressbaricon.body.x = 725-(this.enemynum/15)*270;
        }
        else if(game.global.level == 2){
            this.progressbaricon.body.x = 725-(this.enemynum/25)*270;
        }
        else if(game.global.level == 3){
            this.progressbaricon.body.x = 725-(this.enemynum/35)*270;
        }
        //暫停
        if(pause.isDown){
            if(game.paused==false){
                stateText.text = " pause, \n Press ESC to resume";
                stateText.visible = true;
                game.paused=true;             
            }
        }
        
        //放置植物
        for(var i = 0; i < 6; i++){ //拉植物的位置, 放置成功後 this.gamestart = 1
            if((i==0&&this.money-20>=0)||(i==1&&this.money-30>=0)||(i==2&&this.money-40>=0)||(i==3&&this.money-50>=0)||(i==4&&this.money-40>=0)||(i==5&&this.money-60>=0)){ //看錢夠不夠 
                if(game.time.now > this.plants[i].cooldown)
                this.Placeplants(i); 
            }
        }

        if(this.gamestart){ //this.gamestart = 1 代表地板上面有放置東西, 敵人才產生, 遊戲開始 
            //自動增加錢
            if (this.moneytimer < game.time.now) {
                this.moneytimer = game.time.now + this.moneytimerDelay;
                this.money += 10;
                this.moneyText.text = this.moneyString + this.money;
            }

            //敵人自動進入
            if (this.nextEnemypoolAt < game.time.now && this.enemypool.countDead()>0&&
             ((game.global.level==1 &&this.enemynum<15)||(game.global.level==2 &&this.enemynum<25)||(game.global.level==3 &&this.enemynum<35)) ) {//關卡難度
                this.nextEnemypoolAt = game.time.now + this.enemypoolDelay;
                var enemy1 = this.enemypool.getFirstExists(false);
                var therow = game.rnd.integerInRange(0,4)
                enemy1.reset(800, 160 + therow* 60);
                enemy1.anchor.setTo(0.5, 0.5);
                this.enemynum += 1;
                enemy1.live = 10;//修改為10
                enemy1.row = therow;
                enemy1.body.velocity.x = -30;
                //提醒要進入大波敵人，cd變短
                if(this.enemynum==10){
                    stateText.text ="Warning!! \n lots of enemys coming";
                    stateText.visible=true;
                    this.enemypoolDelay=1000;
                    this.texttime = game.time.now + 1500;
                }
                
            }

            //跳錢植物增加錢
            this.plants[0].forEachAlive(function(moneyplant){
                if(moneyplant.generateMoney < game.time.now){
                    playState.money += 10;
                    playState.moneyText.text = playState.moneyString + playState.money;
                    moneyplant.generateMoney = game.time.now + 4000;
                }
            })

            //找到同一排敵人就開火
            for(var i = 1; i < 4; i++){
                this.enemypool.forEachAlive(function(alien){
                    playState.plants[i].forEachAlive(function(pla){
                        if(pla.row == alien.row){
                            if(pla.body.x<alien.body.x){
                                playState.attack(pla, i);
                            }  
                        }
                    })
                });
            }
            this.specialtrapAttack();

            //走到邊界就判輸
            this.enemypool.forEachAlive(function(alien){
                if(alien.body.x==5){
                    playState.lose();
                }
            })

            //敵人保持移動
            this.enemypool.forEachAlive(function(pla){
                pla.body.velocity.x = -30;
            })

            //敵人吃子彈
            for(var i = 1; i < 4; i++) game.physics.arcade.overlap(this.bullets[i], this.enemypool,this.enemyHit, null, this);

            //敵人殺植物
            for(var i = 0; i < 5; i++)  game.physics.arcade.overlap(this.plants[i], this.enemypool,this.plantsHit, null, this);

            //敵人被黑狗清除
            game.physics.arcade.overlap(this.cars, this.enemypool,this.carsHit, null, this);
            
            //敵人子彈
            for(var i = 0; i < 5; i++) game.physics.arcade.overlap(this.enemyBullets, this.plants[i], this.enemyBulletHitsPlant, null, this);
            //特殊陷阱
            game.physics.arcade.overlap(this.plants[5], this.enemypool, this.specialtrapKill, null, this);

            //顯示框提示升級與等級
            if(game.input.x>=this.upgrade_btn.x && game.input.x<=this.upgrade_btn.x+this.upgrade_btn.width &&
                 game.input.y>=this.upgrade_btn.y && game.input.y<=this.upgrade_btn.y+this.upgrade_btn.height){
                     this.upgradetip();
                }
            else{
                if(tiptext) tiptext.destroy();
                if(tiprec) tiprec.destroy();
            } 

        }
            
    },
    Placeplants:function(i) {
        if(this.plantpull[i].beplaced == 1){
            this.plantpull[i].x = mouseX;
            this.plantpull[i].y = mouseY;
        }
        for(var j=0;j<50;j++){
            if(this.plantpull[i].x - this.squre[j].x < 30 && this.plantpull[i].x - this.squre[j].x >= -30 && this.plantpull[i].y - this.squre[j].y < 30 && this.plantpull[i].y - this.squre[j].y >= -30){
                if(this.plantpull[i].beplaced == 1){  
                    this.plantpull[i].x = this.squre[j].x;
                    this.plantpull[i].y = this.squre[j].y;
                }
                if(this.plantpull[i].beplaced == 2){
                    var flag = 0;
                    //合體
                    if(this.squre[j].thingonit && (i==1||i==2)){
                            var oldpla;
                            this.plants[3].forEachAlive(function(pla){
                                if(pla.squrenum==j){
                                    flag = 1;
                                    oldpla = pla;
                                }
                            });
                                    if(oldpla){   
                                    this.emitter = game.add.emitter(oldpla.body.x+17, oldpla.body.y+25, 15);
                                    this.emitter.makeParticles('effect');
                                    this.emitter.setYSpeed(-50, 50);
                                    this.emitter.setXSpeed(-50, 50);
                                    this.emitter.setScale(2, 0, 2, 0, 800);
                                    this.emitter.gravity = 0;
                                    this.emitter.start(true,800,null,100);
                                    oldpla.kill();

                                    var singleplant;
                                    if(i == 1) {
                                        singleplant = playState.plants[3].create( playState.squre[j].x , playState.squre[j].y, 'mummy1');
                                        singleplant.firepower = 7;
                                        singleplant.version = 1;
                                        this.plants[i].cooldown = game.time.now + 2500;
                                        game.add.tween(this.plantpull[i]).to({angle: -360}, 2500).start();
                                    }    
                                    if(i == 2)  {
                                        singleplant = playState.plants[3].create( playState.squre[j].x , playState.squre[j].y, 'mummy2');
                                        singleplant.firepower = 8.5;
                                        singleplant.version = 2;
                                        this.plants[i].cooldown = game.time.now + 3500;
                                        game.add.tween(this.plantpull[i]).to({angle: -360}, 3500).start();
                                    }
                                    singleplant.inputEnabled = true;
                                    singleplant.events.onInputDown.add(()=>{
                                        if(killMode <0){                      
                                            singleplant.kill();
                                            playState.squre[singleplant.squrenum].thingonit = false;
                                        }
                                    }, this);
                                    singleplant.anchor.setTo(0.5, 0.5);
                                    singleplant.scale.setTo(0.06, 0.06);
                                    singleplant.live = 6;
                                    singleplant.isselected = 0; //選中狀態
                                    singleplant.level = 1; //等級
                                    singleplant.squrenum = j;  //植物在第幾塊地板上
                                    singleplant.bullettime=0;
                                    singleplant.row = (singleplant.squrenum-singleplant.squrenum%10)/10 //植物在哪一排
                                    playState.plantpull[i].x = 100*(i+2);       //放成功後,
                                    playState.plantpull[i].y = 500;
                                    playState.gamestart = true;
                                    if(i == 1){
                                        playState.money -= 30;
                                        playState.moneyText.text = playState.moneyString + playState.money;
                                    }
                                    if(i == 2){
                                        playState.money -= 40;
                                        playState.moneyText.text = playState.moneyString + playState.money;
                                    }
                                }
                    }
                    if(!this.squre[j].thingonit){ //第i塊地板沒被放東西才能被放
                        //放成功後, 把plants的group增加一個植物, 原來可以拉的植物會到原位
                        flag = 1;
                        var singleplant;
                        if(i == 0)  {
                            singleplant = this.plants[i].create( this.squre[j].x , this.squre[j].y, 'ankh');
                            singleplant.generateMoney=0;
                            this.plants[i].cooldown = game.time.now + 2000;
                            game.add.tween(this.plantpull[i]).to({angle: -360}, 2000).start();
                        }
                        if(i == 1) {
                            singleplant = this.plants[i].create( this.squre[j].x , this.squre[j].y, 'beetle');
                            singleplant.angle = 90;
                            singleplant.firepower = 1.5;
                            this.plants[i].cooldown = game.time.now + 2500;
                            game.add.tween(this.plantpull[i]).to({angle: -360}, 2500).start();
                        }    
                        if(i == 2)  {
                            singleplant = this.plants[i].create( this.squre[j].x , this.squre[j].y, 'lion');
                            singleplant.firepower = 3.5;
                            this.plants[i].cooldown = game.time.now + 3500;
                            game.add.tween(this.plantpull[i]).to({angle: -360}, 3500).start();
                        }
                        if(i == 3){
                            singleplant = this.plants[i].create( this.squre[j].x , this.squre[j].y, 'mummy');
                            singleplant.firepower = 5;
                            singleplant.version = 0; //是否合體
                            this.plants[i].cooldown = game.time.now + 4000;
                            game.add.tween(this.plantpull[i]).to({angle: -360}, 4000).start();
                        }
                        if(i == 4) {
                            singleplant = this.plants[i].create( this.squre[j].x , this.squre[j].y, 'coffin');
                            this.plants[i].cooldown = game.time.now + 5000;
                            game.add.tween(this.plantpull[i]).to({angle: -360}, 5000).start();
                        }
                        if(i == 5) {
                            singleplant = this.plants[i].create( this.squre[j].x , this.squre[j].y, 'rock');
                            this.plants[i].cooldown = game.time.now + 8000;
                            game.add.tween(this.plantpull[i]).to({angle: -360}, 8000).start();
                        }

                        game.add.tween(singleplant).to({y: this.squre[j].y-20}, 100).yoyo(true).start();//動畫
                        
                        singleplant.inputEnabled = true;
                        singleplant.events.onInputDown.add(()=>{
                            if(killMode <0){                      
                                singleplant.kill();
                                playState.squre[singleplant.squrenum].thingonit = false;
                            }
                        }, this);
                        singleplant.anchor.setTo(0.5, 0.5);
                        if(i == 0) singleplant.scale.setTo(0.03, 0.025);
                        else if(i<5) singleplant.scale.setTo(0.06, 0.06);
                        if(i == 4) singleplant.live = 15;
                        else singleplant.live = 3;
                        singleplant.isselected = 0; //選中狀態
                        singleplant.level = 1; //等級
                        singleplant.squrenum = j;  //植物在第幾塊地板上
                        singleplant.bullettime = 0;
                        singleplant.row = (singleplant.squrenum-singleplant.squrenum%10)/10 //植物在哪一排
                        this.plantpull[i].x = 100*(i+2);       //放成功後,
                        this.plantpull[i].y = 500;
                        this.gamestart = true;
                        this.squre[j].thingonit = true; // 第i塊地板有被放東西
                        if(i == 0){
                            this.money -= 30;
                            this.moneyText.text = this.moneyString + this.money;
                        }
                        if(i == 1){
                            this.money -= 30;
                            this.moneyText.text = this.moneyString + this.money;
                        }
                        if(i == 2 || i == 4){
                            this.money -= 40;
                            this.moneyText.text = this.moneyString + this.money;
                        }
                        if(i == 3){
                            this.money -= 50;
                            this.moneyText.text = this.moneyString + this.money;
                        }
                        if(i == 5){
                            this.money -= 80;
                            this.moneyText.text = this.moneyString + this.money;
                        }
                        game.time.events.add(200, function(){singleplant.nohurt=0}, this);
                    }
                    if(!flag){
                        this.plantpull[i].x = 100*i+120;
                        this.plantpull[i].y = 500;
                        this.plantpull[i].beplaced = 0;
                    }
                }
            }
            else if(this.plantpull[i].beplaced == 2 && !(this.plantpull[i].x < 700 && this.plantpull[i].x >= 130 && this.plantpull[i].y < 430 && this.plantpull[i].y >= 160)){
                this.plantpull[i].x = 100*i+120;
                this.plantpull[i].y = 500;
                this.plantpull[i].beplaced = 0;
            }
        }
    }, 

    plantsHit:function(plant,alien){
        alien.body.velocity.x=0;
        plant.live -=0.03;

        if(plant.live<=0){
            plant.nohurt = 1;
            this.squre[plant.squrenum].thingonit = false;
            plant.kill();
            alien.body.velocity.x=-60;
            //馬力歐的破碎動畫
            this.emitter = game.add.emitter(plant.body.x, plant.body.y, 15);
            this.emitter.makeParticles('pixel');
            this.emitter.setYSpeed(-150, 150);
            this.emitter.setXSpeed(-150, 150);
            this.emitter.setScale(2, 0, 2, 0, 800);
            this.emitter.gravity = 500;
            this.emitter.start(true,800,null,15);
        }
    },

    carsHit:function(car,enemy){
        car.body.velocity.x=50;
        if(car.hitnum>0){
        car.hitnum--;
        enemy.kill();
        this.emitter = game.add.emitter(enemy.body.x, enemy.body.y, 15);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity = 500;
        this.emitter.start(true,800,null,15);
        }
        else{
        car.kill();
        this.emitter = game.add.emitter(car.body.x, car.body.y, 15);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity = 500;
        this.emitter.start(true,800,null,15);
        }
        if (this.enemypool.countLiving() == 0 && ((game.global.level==1&&this.enemynum==15)||(game.global.level==2&&this.enemynum==25)||(game.global.level==3&&this.enemynum==35))){
            console.log('win');
            this.score += 1000;
            scoreText.text = scoreString + this.score;
            this.texttime = game.time.now +1500;
            this.win();
            game.input.onTap.addOnce(this.restart,this);
        }
    },

    enemyHit:function (bullet, enemy) {
        enemy.live -= bullet.firepower;
        bullet.kill();
        if(enemy.live<=0){
            this.score+=20;
            scoreText.text = scoreString + this.score;
            enemy.kill();
            this.emitter = game.add.emitter(enemy.body.x, enemy.body.y, 15);
            this.emitter.makeParticles('pixel');
            this.emitter.setYSpeed(-150, 150);
            this.emitter.setXSpeed(-150, 150);
            this.emitter.setScale(2, 0, 2, 0, 800);
            this.emitter.gravity = 500;
            this.emitter.start(true,800,null,15);
        }
        if (this.enemypool.countLiving() == 0 && ((game.global.level==1&&this.enemynum==15)||(game.global.level==2&&this.enemynum==25)||(game.global.level==3&&this.enemynum==35))){
            console.log('win');
            this.score += 1000;
            scoreText.text = scoreString + this.score;
            this.texttime = game.time.now +1500;
            this.win();
            game.input.onTap.addOnce(this.restart,this);

        }
    },

    attack:function(plant,i){
        if (game.time.now > plant.bullettime){
            var bullet1 = this.bullets[i].getFirstExists(false);
            if (bullet1){
                bullet1.reset(plant.x + 38,plant.y);
                bullet1.body.velocity.x = 80;
                plant.bullettime = game.time.now + 2000;
                bullet1.firepower = plant.firepower;//殺傷力
            }
        }
    },

    upgradetip: function(){
        if(showSelect){
            for(var k = 1; k < 5; k++){
                this.plants[k].forEachAlive(function(pla){
                    if(pla.isselected){
                        if(k==1) playState.bmd = game.add.bitmapData(120, 24);
                        if(k==2) playState.bmd = game.add.bitmapData(110, 24);
                        if(k==3) {
                            if(pla.version!=0) playState.bmd = game.add.bitmapData(145, 24);
                            else playState.bmd = game.add.bitmapData(135, 24);
                        }
                        if(k==4) playState.bmd = game.add.bitmapData(120, 24);
                        playState.bmd.ctx.beginPath();
                        playState.bmd.ctx.rect(0, 0, playState.bmd.width, playState.bmd.height);
                        playState.bmd.ctx.fillStyle = '#ffffff';
                        playState.bmd.ctx.fill();

                        if(tiprec) tiprec.destroy();
                        tiprec = game.add.sprite(playState.upgrade_btn.x-25, playState.upgrade_btn.y-24, playState.bmd);

                        if(tiptext) tiptext.destroy();
                        if(pla.level<3){
                        if(k==3 && pla.version==1)
                        tiptext = game.add.text(playState.upgrade_btn.x-23, playState.upgrade_btn.y-18, playState.plantName[k]+"*1"+" 等級:"+pla.level+" 可升級", { font: '12px Palatino', fill: '#000000' });
                        else if(k==3 && pla.version==2)
                        tiptext = game.add.text(playState.upgrade_btn.x-23, playState.upgrade_btn.y-18, playState.plantName[k]+"*2"+" 等級:"+pla.level+" 可升級", { font: '12px Palatino', fill: '#000000' });
                        else
                        tiptext = game.add.text(playState.upgrade_btn.x-23, playState.upgrade_btn.y-18, playState.plantName[k]+" 等級:"+pla.level+" 可升級", { font: '12px Palatino', fill: '#000000' });
                        }
                        else{
                        if(k==3 && pla.version==1)
                        tiptext = game.add.text(playState.upgrade_btn.x-23, playState.upgrade_btn.y-18, playState.plantName[k]+"*1"+" 等級:"+pla.level+" 不可升級", { font: '12px Palatino', fill: '#000000' });
                        else if(k==3 && pla.version==2)
                        tiptext = game.add.text(playState.upgrade_btn.x-23, playState.upgrade_btn.y-18, playState.plantName[k]+"*2"+" 等級:"+pla.level+" 不可升級", { font: '12px Palatino', fill: '#000000' });
                        else
                        tiptext = game.add.text(playState.upgrade_btn.x-23, playState.upgrade_btn.y-18, playState.plantName[k]+" 等級:"+pla.level+" 不可升級", { font: '12px Palatino', fill: '#000000' });
                        }
                    }
                });
            }
        }
    },

    enemyBulletHitsPlant: function (bullet,plant) {
        if(plant.nohurt == 0){
            bullet.kill();
            plant.live -= 0.5;
            if(plant.live<=0){
                plant.nohurt = 1;
                plant.kill();
                this.squre[plant.squrenum].thingonit = false;
                //馬力歐的破碎動畫
                this.emitter = game.add.emitter(plant.body.x, plant.body.y, 15);
                this.emitter.makeParticles('pixel');
                this.emitter.setYSpeed(-150, 150);
                this.emitter.setXSpeed(-150, 150);
                this.emitter.setScale(2, 0, 2, 0, 800);
                this.emitter.gravity = 500;
                this.emitter.start(true,800,null,15);
            }
        }
    },
    enemyFires: function  () {
        enemyBullet = this.enemyBullets.getFirstExists(false);
        if(enemyBullet && this.enemypool.countLiving()>0){
            if(this.firingTimer!=0) //排除第一次攻擊
            this.enemypool.forEachAlive(function(enemy){
                playState.enemyFires2(enemy);
        });  
        if(game.global.level==1) //關卡難度
        this.firingTimer = game.time.now + 12000;
        else if(game.global.level==2)
        this.firingTimer = game.time.now + 9000;
        else if(game.global.level==3)
        this.firingTimer = game.time.now + 8000;
    }
    },
    enemyFires2: function  (enemy) {
        enemyBullet = this.enemyBullets.getFirstExists(false);
        if(!enemyBullet) {return;}
        enemyBullet.reset(enemy.body.x+39, enemy.body.y+30);
        enemyBullet.body.velocity.x = -80;
    },
    specialtrapAttack: function (){
        playState.plants[5].forEachAlive(function(pla){
            playState.enemypool.forEachAlive(function(alien){
                if(pla.row == alien.row){
                    if(pla.body.x+60>alien.body.x){
                        pla.body.velocity.x=100;
                        game.time.events.add(620, function(){
                        pla.kill();
                        playState.squre[pla.squrenum].thingonit = false;
                        }, this);
                    }  
                }
            })
        });
    },
    specialtrapKill:function (bullet, enemy) {
        this.score+=20;
        scoreText.text = scoreString + this.score;
        enemy.kill();
        this.emitter = game.add.emitter(enemy.body.x, enemy.body.y, 15);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity = 500;
        this.emitter.start(true,800,null,15);
        if (this.enemypool.countLiving() == 0){
            console.log('win');
            this.score += 1000;
            scoreText.text = scoreString + this.score;
            this.texttime = game.time.now +1500;
            this.win();
            game.input.onTap.addOnce(this.restart,this);
        }
    },

    lose:function(){
        this.enemyBullets.callAll('kill');
        this.enemypool.removeAll();
        this.cars.removeAll();
        for(var j=1;j<4;j++){
            this.bullets[j].removeAll();
        }
        this.score=0;
        this.enemynum=0;
        this.gamestart = false;
        stateText.text ="You lose!!";
        stateText.visible=true;
        for(var i=0;i<6;i++){
            this.plants[i].removeAll();
        }
        this.texttime=game.time.now + 1500;
        game.global.win = 0;
        game.input.onTap.addOnce(this.restart,this);
    },
    win:function(){
        this.enemyBullets.callAll('kill');
        this.enemypool.removeAll();
        this.cars.removeAll();
        for(var j=1;j<4;j++){
            this.bullets[j].removeAll();
        }
        this.score=0;
        this.enemynum=0;
        this.gamestart = false;
        stateText.text ="You win!!";
        stateText.visible=true;
        for(var i=0;i<6;i++){
            this.plants[i].removeAll();
        }
        this.texttime=game.time.now + 1500;
        game.input.onTap.addOnce(this.restart,this);
    }, 

    restart:function(){
        game.state.start('end');
    },

}