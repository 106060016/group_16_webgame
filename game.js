// Initialize Phaser
var game = new Phaser.Game(800, 600, Phaser.AUTO, 'canvas');
// Define our global variable
game.global = { score: 0, level: 0, win: 1};
// Add all the states
game.state.add('load', loadState);
game.state.add('start', startState);
game.state.add('play', playState);
game.state.add('end', endState);
game.state.add('quit', quitState);
// Start the 'load' state
game.state.start('load');