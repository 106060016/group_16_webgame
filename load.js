var loadState = { 
    preload:function(){
        game.load.image('ankh','pictures/ankh.png');
        game.load.image('beetle','pictures/beetle.png');
        game.load.image('lion','pictures/lion.png');
        game.load.image('mummy','pictures/mummy.png');
        game.load.image('coffin','pictures/coffin.png');
        game.load.image('rock','pictures/rock.png');
        
        game.load.image('beetle_bullet','pictures/beetle_bullet.png');
        game.load.image('lion_bullet','pictures/lion_bullet.png');
        game.load.image('mummy_bullet','pictures/mummy_bullet.png');

        game.load.image('background', 'pictures/background.jpg');
        game.load.image('car', 'pictures/car.png');
        game.load.image('floor', 'pictures/floor.jpg');
        game.load.image('enemy', 'pictures/enemy.png');

        game.load.image('pixel', 'pictures/pixel.png');
        game.load.image('select', 'pictures/select.png');
        game.load.spritesheet('upgrade_btn', 'pictures/buttonsheet.png', 90,50);
        game.load.image('mummy1','pictures/mummy_2.png');
        game.load.image('mummy2','pictures/mummy_3.png');
        game.load.image('effect','pictures/effect.png');

        game.load.image('shovel', 'pictures/shovel.png');
        game.load.image('delete', 'pictures/delete.png');
        game.load.image('enemybullet', 'pictures/enemybullet3.png');

        game.load.image('progressbar','pictures/progressbar.png');
        game.load.image('coin','pictures/coin.png');

        game.load.image('word-background', 'pictures/word-background.png');
    },
    create: function() {
        game.state.start('start');
    }
};