var quitState = { 
    preload:function(){
    },
    create: function() {
    // Add a background image 
    this.background = game.add.sprite(game.world.centerX,game.world.centerY, 'background');
    this.background.anchor.setTo(0.5,0.5);
    this.background.scale.setTo(0.5,0.5);

    var wordLabel = game.add.text(game.width/2, game.height/2, "you've quit the game", { font: '60px Palatino', fill: '#ffd700' }); 
    wordLabel.anchor.setTo(0.5, 0.5);
    }
};