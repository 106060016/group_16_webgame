
var startState={
    preload:function(){
    game.load.image('background', 'pictures/background.jpg');
   
    },
    create: function() {
        this.background = game.add.sprite(game.world.centerX,game.world.centerY, 'background');
        this.background.anchor.setTo(0.5,0.5);
        this.background.scale.setTo(0.5,0.5);
        var nameLabel = game.add.text(game.width/2, 180, '趕走掘墓者', { font: '80px Palatino', fill: '#ffd700' }); 
        nameLabel.anchor.setTo(0.5, 0.5);
        
        if(game.global.win){
        if(game.global.level<3) game.global.level++;
        else game.global.level = 1;
        }
        else{
        game.global.win = 1;
        game.global.level = 1;
        }

        this.startLabel = game.add.text(game.width/2, 350, 'Start', { font: '50px Palatino', fill: '#ffd700' }); 
        this.startLabel.anchor.setTo(0.5, 0.5);
        
        this.startLabel.inputEnabled = true;
        this.startLabel.events.onInputUp.add(this.start);

        this.wordBackground = game.add.sprite(50, 380, 'word-background');
        this.wordBackground.alpha = 0.6;
        this.line1Label = game.add.text(game.width/2, 400, '拖拽任意生物到圖中開始遊戲', { font: '25px Palatino', fill: '#000000' }); 
        this.line1Label.anchor.setTo(0.5, 0.5);
        this.line2Label = game.add.text(game.width/2, 440, '第二至五種生物可升級，選中生物後點升級鍵', { font: '25px Palatino', fill: '#000000' }); 
        this.line2Label.anchor.setTo(0.5, 0.5);
        this.line3Label = game.add.text(game.width/2, 480, '拖拽第二種或第三種生物到圖中的第四種生物，可合體', { font: '25px Palatino', fill: '#000000' }); 
        this.line3Label.anchor.setTo(0.5, 0.5);
        this.line4Label = game.add.text(game.width/2, 520, '點鋤具，可剷除生物', { font: '25px Palatino', fill: '#000000' }); 
        this.line4Label.anchor.setTo(0.5, 0.5);
        this.line5Label = game.add.text(game.width/2, 560, '按ESC鍵暫停，再按ESC鍵繼續遊戲', { font: '25px Palatino', fill: '#000000' }); 
        this.line5Label.anchor.setTo(0.5, 0.5);
    
        },
        start: function() {
        game.state.start('play');
        }, 
}