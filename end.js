var endState = { 
    preload:function(){
    game.load.image('background', 'pictures/background.jpg');
    },
    create: function() {
    this.background = game.add.sprite(game.world.centerX,game.world.centerY, 'background');
    this.background.anchor.setTo(0.5,0.5);
    this.background.scale.setTo(0.5,0.5);

    if(game.global.win && game.global.level==3){
    game.global.level = 1;
    var winLabel = game.add.text(game.width/2, 250, 'You win!', { font: '70px Palatino', fill: '#ffd700' }); 
    winLabel.anchor.setTo(0.5, 0.5);
    var restartLabel = game.add.text(game.width/2, 380, 'restart', { font: '50px Palatino', fill: '#ffd700' }); 
    restartLabel.anchor.setTo(0.5, 0.5);
    restartLabel.inputEnabled = true;
    restartLabel.events.onInputUp.add(this.restart);
    }
    else if(game.global.win){
    game.global.level++;
    var playLabel = game.add.text(game.width/2, 250, 'Play Next Level', { font: '60px Palatino', fill: '#ffd700' }); 
    playLabel.anchor.setTo(0.5, 0.5);
    playLabel.inputEnabled = true;
    playLabel.events.onInputUp.add(this.play);
    }
    else{
    game.global.win = 1;
    game.global.level = 1;
    var winLabel = game.add.text(game.width/2, 250, 'You lose!', { font: '70px Palatino', fill: '#ffd700' }); 
    winLabel.anchor.setTo(0.5, 0.5);
    var restartLabel = game.add.text(game.width/2, 380, 'restart', { font: '50px Palatino', fill: '#ffd700' }); 
    restartLabel.anchor.setTo(0.5, 0.5);
    restartLabel.inputEnabled = true;
    restartLabel.events.onInputUp.add(this.restart);
    }
    var quitLabel = game.add.text(700, 520, 'quit', { font: '45px Palatino', fill: '#ffd700' }); 
    quitLabel.anchor.setTo(0.5, 0.5);

    quitLabel.inputEnabled = true;
    quitLabel.events.onInputUp.add(this.quit);

    },
    restart: function() {
        game.state.start('start');
    },
    play: function() {
        game.state.start('play');
    },
    quit: function() {
        game.state.start('quit');
    },
}